var player1;
var player2;
var ball;
var start = false;

/**
 * Démmarage du jeu,
 * instanciation de toutes les variables
 */
function startGame(){
    player1 = new Player("Joueur 1", "left");
    player2 = new Player("Joueur 2", "right");
    ball = new Ball("red");
    game.firstDraw();
    //game.start();
}

var game = {

    canvas: document.createElement("canvas"),
    init: function(){
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight - 10;

        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    },
    start: function(){
        this.init();
        ball.launch();
        this.interval = setInterval(updateGame, 20);
    },
    firstDraw: function(){
        this.init();
        this.draw();
        player1.reset();
        player2.reset();
        ball.draw();
    },
    draw: function(){
        //On draw les traits
        /*
        Bar height: 50; width; 10
         */
        for(var i = 0; i < 10; i++){
            var xB = this.canvas.width / 2 - 2.5;
            var yB = 10 + (i * 50) + (i * 30);
            this.context.fillStyle = "black";
            this.context.fillRect(xB, yB, 5, 50);
        }

    },
    clear: function(){
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    stop: function(){
        clearInterval(this.interval);
        this.clear();
    },
    pause: function(){
        clearInterval(this.interval);
        start = false;
    },
    restart: function(){
        this.stop();
        this.draw();
		player1.reset();
        player2.reset();
        ball.reset();
		start = false;
    }

};

/**
 * Boucle du jeu
 */
function updateGame() {
    game.clear();
    game.draw();
    player1.update();
    player2.update();
    ball.update();
}

function keyDown(e){
    var key = e.keyCode;

    //Commencer la partie avec entrée
    if(key === 32 && !start){
        start = true;
        game.start();
    }

    if(start) {
        if (key === 38) {
            player2.keypressed = true;
            player2.keyactive = "up";
        } else if (key === 40) {
            player2.keypressed = true;
            player2.keyactive = "down";
        }

        if (key === 90) {
            player1.keypressed = true;
            player1.keyactive = "up";
        } else if (key === 83) {
            player1.keypressed = true;
            player1.keyactive = "down";
        }
    }
}
function keyUp(e){
    var key = e.which;
   // alert("keyup:" + key);

    if(start) {
        if (key === 38 || key === 40) {
            player2.keypressed = false;
            player2.keyactive = "";
        }

        if (key === 90 || key === 83) {
            player1.keypressed = false;
            player1.keyactive = "";
        }
    }
}

/**
 * Class of player
 * @param name
 * @param side
 * @constructor
 */
function Player(name, side){
    this.name = name;
    this.side = side;
    this.point = 0;

    this.width = 25;
    this.height = 200;

    this.x = side === "right" ? window.innerWidth - 25 : 0;
    this.y = window.innerHeight/2 - 100;

    this.keyactive = "";
    this.keypressed = false;

    this.speed = 8;
	
	this.reset = function() {
		this.x = side === "right" ? window.innerWidth - 25 : 0;
		this.y = window.innerHeight/2 - 100;
		
		this.draw();
	}

    /**
     * Up the bar
     */
    this.up = function(){
        if(this.y > 0)
            this.y -= this.speed;
        else
            this.keypressed = false;
    };
    /**
     * Down the bar
     */
    this.down = function(){
        if(this.y < window.innerHeight - this.height - 20)
            this.y += this.speed;
        else
            this.keypressed = false;
    };

    this.draw = function(){
        var ctx = game.context;
        ctx.fillStyle = "black";
        ctx.fillRect(this.x, this.y, this.width, this.height);
    };

    /**
     * Update the player
     */
    this.update = function(){
        this.draw();

        switch (this.keyactive){
            case "up":
                this.up();
                break;
            case "down":
                this.down();
                break;
            default:
                this.keypressed =false;
                break;
        }
    };

}

/**
 * Class of ball
 * @param color
 * @constructor
 */
function Ball(color){
    this.pi = Math.PI;
    this.radius = 16;

    this.velocity = null;
    this.speed = 8;

    this.x = window.innerWidth / 2 - 10;
    this.y = window.innerHeight / 2 - 10 - 10;

	this.reset = function() {
		this.x = window.innerWidth / 2 - 10;
		this.y = window.innerHeight / 2 - 10 - 10;
		this.speed = 8;
		this.velocity = null;
		this.draw();
	}
		
    this.draw = function(){
        var ctx = game.context;
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.radius, this.radius);
    };

    this.update = function(){
        this.draw();

        this.x += this.velocity.x;
        this.y += this.velocity.y;

        if(this.y < 0 || this.y+this.radius > (window.innerHeight-10)){
            var offset = this.velocity.y < 0 ? 0 - this.y : window.innerHeight - 10 - (this.y+this.radius);
            this.y += 2*offset;
            this.velocity.y *= -1;
        }

        /**
         * Check the hitbox
         * @return {boolean}
         */
        var AABBIntersect = function(ax, ay, aw, ah, bx, by, bw, bh){
            return ax < bx+bw && ay < by+bh && bx < ax+aw && by < ay+ah;
        };


        //Player handler
        var pdle = this.velocity.x < 0 ? player1 : player2;
        if(AABBIntersect(pdle.x, pdle.y, pdle.width, pdle.height,
                this.x, this.y, this.radius, this.radius)){
            var n = (this.y + this.radius - pdle.y) / (pdle.height + this.radius);
            var phi = 0.25*this.pi*(2*n-1); // pi/4 = 45
            this.velocity.x = (pdle === player1 ? 1 : -1)*this.speed*Math.cos(phi);
            this.velocity.y = this.speed*Math.sin(phi);
			this.speed = this.speed + 1.25;
        }

        var winner = false;

        if(pdle === player1 && this.x < pdle.x) {
            //winner 2
            player2.point++;
            document.getElementById("j2p").innerText = player2.point.toString();
            winner = true;
        } else if(pdle === player2 && this.x > pdle.x+pdle.width) {
            //Winner 1
            player1.point++;
            document.getElementById("j1p").innerText = player1.point.toString();
            winner = true;
        }

        if(winner){
            game.restart();
        }

    };


    this.launch = function(){
        this.velocity = {
            x: this.speed,
            y: 0
        }
    }
}